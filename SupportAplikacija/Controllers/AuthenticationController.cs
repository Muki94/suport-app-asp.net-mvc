﻿using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SupportAplikacija.Helper;

namespace SupportAplikacija.Controllers
{
    public class AuthenticationController : Controller
    {
        SupportContext context = new SupportContext();

        public ActionResult Index()
        {
            Session["korisnik"] = null;

            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                return Redirect("/Klijent/Index");
            }else{
                FormsAuthentication.SignOut();

                HttpCookie myCookie = new HttpCookie("korisnik");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            return View();
        }

        public ActionResult Login(string username, string password, string remember)
        {
            string hashPassword = Helpers.Hash(password);
            Korisnik korisnik = context.Korisnik.Where(x => ((x.UserName == username || x.Email == username) && x.Password == hashPassword)).SingleOrDefault();

            if (korisnik != null)
            {
                //Postavljanje auth cookia 
                FormsAuthentication.SetAuthCookie(korisnik.Email, false);

                var authTicket = new FormsAuthenticationTicket(1, korisnik.Email, DateTime.Now, DateTime.Now.AddMinutes(3), false, "User");
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Response.Cookies.Add(authCookie);

                //Postavljanje cooki-a
                HttpCookie cookie = new HttpCookie("korisnik");
                cookie.Values.Add("id", korisnik.KorisnikId.ToString());
                cookie.Values.Add("korisnickoIme", korisnik.Ime);
                cookie.Expires.AddMinutes(3);
                HttpContext.Response.Cookies.Add(cookie);

                //Postavljanje sesije
                HttpContext.Session["korisnik"] = korisnik;
                HttpContext.Session["korisnickoIme"] = korisnik.Ime;

                return RedirectToAction("Index", "Klijent");
            }
            else
            {
                ViewBag.error = "Korisničko ime ili lozinka nisu ispravni!";
                return View("Index");
            }
        }

        public ActionResult LogOut() {
            //Ciscenje sesije
            HttpContext.Session["korisnik"] = null;

            //Ciscenje cooki-a
            HttpCookie myCookie = new HttpCookie("korisnik");
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(myCookie);

            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Authentication");
        }
    }
}
