﻿using DAL;
using DAL.Models;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace SupportAplikacija.Controllers
{
    [Authorize]
    public class SlikaController : Controller
    {
        SupportContext context = new SupportContext();

        // GET: Slika
        public ActionResult Index(int klijentId)
        {
            try
            {
                    List<SlikaVM> slike = context.Slika.Select(x => new SlikaVM
                    {
                        KlijentId = x.KlijentId,
                        PutanjaDoSlike = x.PutanjaDoSlike,
                        Napomena = x.Napomena,
                        SlikaId = x.SlikaId
                    }).Where(x => x.KlijentId == klijentId)
                      .ToList();

                    ViewBag.klijentId = klijentId;

                    return PartialView("Index_Partial", slike);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // GET: Slika/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                    SlikaVM slika = context.Slika.Where(x => x.SlikaId == id).Select(x => new SlikaVM
                    {
                        KlijentId = x.KlijentId,
                        Napomena = x.Napomena,
                        PutanjaDoSlike = x.PutanjaDoSlike,
                        SlikaId = x.SlikaId

                    }).FirstOrDefault();

                    return PartialView("Details_Partial", slika);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // POST: Slika/Create
        [HttpPost]
        public ActionResult Create(HttpPostedFileBase file, string opis, int klijentId, int slikaId = 0)
        {
            try
            {
                    Slika slikaZaDodati;

                    if (slikaId != 0)
                    {
                        slikaZaDodati = context.Slika.Where(x => x.SlikaId == slikaId).FirstOrDefault();
                        slikaZaDodati.Napomena = opis != null ? opis : slikaZaDodati.Napomena;
                    }
                    else
                    {
                        slikaZaDodati = new Slika();
                        context.Slika.Add(slikaZaDodati);
                        slikaZaDodati.Napomena = opis;
                    }

                    if (file != null)
                    {
                        string klijent = context.Klijent.Where(x => x.KlijentId == klijentId).FirstOrDefault().Ime.Replace(' ', '_');

                        string slika = Path.GetFileName(file.FileName);
                        string ekstenzijaSlike = Path.GetExtension(file.FileName);

                        //generisanje unikatnog imena slici
                        slika = Guid.NewGuid().ToString() + ekstenzijaSlike;

                        System.IO.Directory.CreateDirectory(Path.Combine(Server.MapPath("~/Images/"), klijent.ToString()));
                        string path = Path.Combine(Server.MapPath("~/Images/" + klijent + "/"), slika);

                        WebImage img = new WebImage(file.InputStream);
                        if (img.Width > 400 || img.Height > 400)
                            img.Resize(350, 350);
                        img.Save(path);

                        slikaZaDodati.PutanjaDoSlike = "/Images/" + klijent.ToString() + "/" + slika;
                        slikaZaDodati.KlijentId = klijentId;
                    }

                    context.SaveChanges();

                    return Redirect("/Klijent/Details/" + klijentId);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // POST: Slika/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, int klijentId)
        {
            try
            {
                    Slika slika = context.Slika.Where(x => x.SlikaId == id).FirstOrDefault();
                    string fullPath = Request.MapPath(slika.PutanjaDoSlike);
                    System.IO.File.Delete(fullPath);

                    context.Slika.Remove(slika);
                    context.SaveChanges();
                    return RedirectToAction("Index", new { klijentId = klijentId });
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }
    }
}
