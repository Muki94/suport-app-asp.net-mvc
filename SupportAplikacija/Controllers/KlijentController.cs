﻿using DAL;
using DAL.Models;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SupportAplikacija.Controllers
{
    [Authorize]
    public class KlijentController : Controller
    {
        SupportContext context = new SupportContext();

        // GET: /Klijent/
        public ActionResult Index(string searchInput = "", string dateFrom = "", string dateTo = "", int itemsPerPage = 25, int currentPage = 0)
        {
            try
            {
                List<KlijentVM> klijenti = context.Klijent.Select(x => new KlijentVM
                {
                    KlijentId = x.KlijentId,
                    Ime = x.Ime,
                    Alias = x.Alias,
                    Adresa = x.Adresa,
                    DatumDodavanja = x.DatumDodavanja,
                    Telefon = x.Telefon,
                    Grad = x.Grad.Naziv,
                    DatumUredjivanja = x.DatumUredjivanja
                }).OrderByDescending(x => x.DatumDodavanja)
                  .ToList();

                if (searchInput != "")
                {
                    klijenti = klijenti.Where(x => x.Ime.Contains(searchInput) ||
                                              x.Alias.Contains(searchInput) ||
                                              x.Telefon.Contains(searchInput) ||
                                              x.Grad.Contains(searchInput) ||
                                              x.Adresa.Contains(searchInput)).ToList();
                }

                if (dateFrom != "")
                {
                    klijenti = klijenti.Where(x => x.DatumDodavanja.Date >= DateTime.Parse(dateFrom).Date).ToList();
                }

                if (dateTo != "")
                {
                    klijenti = klijenti.Where(x => x.DatumDodavanja.Date <= DateTime.Parse(dateTo).Date).ToList();
                }

                //pagging calculations (number of pages)
                decimal numberOfPages = (decimal)klijenti.Count / (decimal)itemsPerPage;
                ViewBag.numberOfPages = Math.Ceiling(numberOfPages);

                //set current page to make it visible on the html
                ViewBag.currentPage = currentPage;

                //pagging calculations (skip and take elements)
                klijenti = klijenti.Skip(currentPage * itemsPerPage).Take(itemsPerPage).ToList();

                //spašavanje trenutnih vrijednosti filtera kako se nebi obrisali nakon pretrage
                ViewBag.searchInput = searchInput;
                ViewBag.dateFrom = dateFrom;
                ViewBag.dateTo = dateTo;
                Session["Selected"] = "KlijentIndex";

                return View(klijenti);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        //
        // GET: /Klijent/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                KlijentVM klijent = context.Klijent.Select(x => new KlijentVM
                {
                    KlijentId = x.KlijentId,
                    Ime = x.Ime,
                    Alias = x.Alias,
                    Telefon = x.Telefon,
                    Grad = x.Grad.Naziv,
                    Adresa = x.Adresa,
                    DatumDodavanja = x.DatumDodavanja,
                    DatumUredjivanja = x.DatumUredjivanja,
                    Napomena = x.Napomena
                }).Where(x => x.KlijentId == id).FirstOrDefault();

                return View(klijent);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        //
        // GET: /Klijent/Create
        public ActionResult Create(int id = 0, string error = null)
        {
            try
            {
                ViewBag.error = error;

                KlijentCreateVM klijentVM = new KlijentCreateVM()
                {
                    Gradovi = context.Grad.Select(x => new SelectListItem
                    {
                        Text = x.Naziv,
                        Value = x.GradId.ToString()
                    }).ToList()
                };

                if (id != 0)
                {
                    Klijent klijent = context.Klijent.Where(x => x.KlijentId == id).FirstOrDefault();
                    klijentVM.Adresa = klijent.Adresa;
                    klijentVM.Ime = klijent.Ime;
                    klijentVM.Alias = klijent.Alias;
                    klijentVM.Telefon = klijent.Telefon;
                    klijentVM.GradId = klijent.GradId;
                    klijentVM.KlijentId = klijent.KlijentId;
                    klijentVM.Napomena = klijent.Napomena;
                }

                return View(klijentVM);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        //
        // POST: /Klijent/Create
        [HttpPost]
        public ActionResult Create(KlijentCreateVM klijentVM)
        {
            try
            {
                List<string> allErrors = new List<string>();
                Klijent klijent = context.Klijent.Where(x => x.Ime.Equals(klijentVM.Ime) &&
                                                             x.Alias.Equals(klijentVM.Alias) &&
                                                             x.Napomena.Equals(klijentVM.Napomena) &&
                                                             x.Telefon.Equals(klijentVM.Telefon) &&
                                                             x.Adresa.Equals(klijentVM.Adresa) &&
                                                             x.GradId == klijentVM.GradId).FirstOrDefault();

                if (klijent != null)
                {
                    Response.AppendHeader("X-Error", "true");
                    allErrors.Add("Klijent već postoji!");
                    return Json(allErrors);
                }

                if (klijentVM.KlijentId == 0)
                {

                    //Kreira novog klijenta i odma ga posprema u bazu 
                    klijent = new Klijent();
                    klijent.DatumDodavanja = DateTime.Now;
                    klijent.DatumUredjivanja = DateTime.Now;
                    context.Klijent.Add(klijent);
                }
                else
                {
                    //Vadi postojeceg klijenta za editovanje 
                    klijent = context.Klijent.Where(x => x.KlijentId == klijentVM.KlijentId).FirstOrDefault();
                    klijent.DatumUredjivanja = DateTime.Now;
                }

                #region Provjera da li je sve ispravno uneseno
                if (!ModelState.IsValid)
                {
                    klijentVM.Gradovi = context.Grad.Select(x => new SelectListItem
                    {
                        Text = x.Naziv,
                        Value = x.GradId.ToString()
                    }).ToList();

                    Response.AppendHeader("X-Error", "true");

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            allErrors.Add(error.ErrorMessage.ToString());
                        }
                    }

                    return Json(allErrors);
                }
                #endregion

                klijent.Ime = klijentVM.Ime;
                klijent.Alias = klijentVM.Alias;
                klijent.GradId = klijentVM.GradId;
                klijent.Telefon = klijentVM.Telefon;
                klijent.Adresa = klijentVM.Adresa;
                klijent.Napomena = klijentVM.Napomena;

                context.SaveChanges();

                return Redirect("/Klijent/Index");
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        //
        // GET: /Klijent/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                context.Klijent.Remove(context.Klijent.Where(x => x.KlijentId == id).SingleOrDefault());
                context.SaveChanges();
                return Redirect("/Klijent/Index");
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        [HttpGet]
        public ActionResult CreateGrad()
        {
            try
            {
                GradCreateVM gradVM = new GradCreateVM();

                return PartialView("CreateGrad_Partial", gradVM);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        [HttpPost]
        public ActionResult CreateGrad(GradCreateVM gradVM)
        {
            try
            {
                List<string> allErrors = new List<string>();
                Grad grad = context.Grad.Where(x => x.Naziv.Equals(gradVM.Naziv.Trim())).FirstOrDefault();

                if (grad != null)
                {
                    Response.AppendHeader("X-Error", "true");
                    allErrors.Add("Grad već postoji!");
                    return Json(allErrors);
                }

                if (gradVM.Naziv == String.Empty)
                {
                    Response.AppendHeader("X-Error", "true");
                    allErrors.Add("Naziv ne može biti prazan!");
                    return Json(allErrors);
                }

                grad = new Grad();
                context.Grad.Add(grad);
                grad.Naziv = gradVM.Naziv.Trim();
                context.SaveChanges();

                return Json(grad);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }
    }
}
