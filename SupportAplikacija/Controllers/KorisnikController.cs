﻿using DAL;
using DAL.Models;
using DAL.ViewModel;
using SupportAplikacija.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SupportAplikacija.Controllers
{
    [Authorize]
    public class KorisnikController : Controller
    {
        SupportContext context = new SupportContext();

        // GET: /Korisnik/
        public ActionResult Index()
        {
            try
            {
                    int idKorisnika = HttpContext.Session["korisnik"] != null ? ((Korisnik)HttpContext.Session["korisnik"]).KorisnikId :Int32.Parse(Request.Cookies["korisnik"]["id"]);
                    KorisnikVM korisnik = context.Korisnik.Where(x => x.KorisnikId == idKorisnika)
                                                          .Select(x => new KorisnikVM
                                                            {
                                                                KorisnikId = x.KorisnikId,
                                                                Ime = x.Ime,
                                                                Prezime = x.Prezime,
                                                                Email = x.Email,
                                                                Password = x.Password,
                                                                Telefon = x.Telefon,
                                                                UserName = x.UserName
                                                            }).FirstOrDefault();

                    Session["Selected"] = "KorisnikIndex";
                    return View(korisnik);
            }
            catch(Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        //
        // GET: /Korisnik/Create
        public ActionResult Create()
        {
            try
            {
                    int idKorisnika = HttpContext.Session["korisnik"] != null ? ((Korisnik)HttpContext.Session["korisnik"]).KorisnikId : Int32.Parse(Request.Cookies["korisnik"]["id"]);
                    KorisnikCreateVM korisnik = context.Korisnik.Where(x => x.KorisnikId == idKorisnika).Select(x => new KorisnikCreateVM
                    {
                        KorisnikId = x.KorisnikId,
                        Ime = x.Ime,
                        Prezime = x.Prezime,
                        Email = x.Email,
                        Telefon = x.Telefon,
                        Password = x.Password,
                        UserName = x.UserName
                    }).FirstOrDefault();

                    ViewBag.allErrors = null;

                    return PartialView("Create",korisnik);
            }
            catch(Exception exception){
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        //
        // POST: /Korisnik/Create
        [HttpPost]
        public ActionResult Create(KorisnikCreateVM korisnikVM)
        {
            try
            {
                Korisnik korisnik = context.Korisnik.Where(x => x.KorisnikId == korisnikVM.KorisnikId).FirstOrDefault();
                List<string> allErrors = new List<string>();

                #region provjera ispravnosti unesenih podataka
                if (!ModelState.IsValid)
                {
                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            allErrors.Add(error.ErrorMessage.ToString());
                        }
                    }

                    ViewBag.allErrors = allErrors;

                    return PartialView("Create",korisnikVM);
                }
                #endregion

                #region Provjera pasvorda
                if (korisnikVM.StariPassword != null)
                {
                    if (!korisnik.Password.Equals(Helpers.Hash(korisnikVM.StariPassword)) && korisnikVM.StariPassword != null)
                    {
                        allErrors.Add("Stari password je neispravan!");
                        ViewBag.allErrors = allErrors;

                        return PartialView("Create", korisnikVM);
                    }
                    else
                    {
                        korisnik.Password = Helpers.Hash(korisnikVM.Password);
                    }
                }
                else
                {
                    korisnik.Password = korisnik.Password;
                }
                #endregion

                korisnik.Ime = korisnikVM.Ime;
                korisnik.Prezime = korisnikVM.Prezime;
                korisnik.UserName = korisnikVM.UserName;
                korisnik.Telefon = korisnikVM.Telefon;
                korisnik.Email = korisnikVM.Email;

                context.SaveChanges();

                KorisnikVM korisnikView = new KorisnikVM();
                korisnikView.KorisnikId = korisnik.KorisnikId;
                korisnikView.Ime = korisnik.Ime;
                korisnikView.Prezime = korisnik.Prezime;
                korisnikView.Email = korisnik.Email;
                korisnikView.Telefon = korisnik.Telefon;
                korisnikView.UserName = korisnik.UserName;
                korisnikView.Password = korisnik.Password;

                return PartialView("Index_Partial", korisnikView);
            }
            catch(Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }
    }
}
