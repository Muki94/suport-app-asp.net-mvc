﻿using DAL;
using DAL.Models;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SupportAplikacija.Controllers
{
    [Authorize]
    public class KasaController : Controller
    {
        SupportContext context = new SupportContext();
        // GET: Kasa
        public ActionResult Index(string searchInput = "", int itemsPerPage = 25, int currentPage = 0, int klijentId = 0)
        {
            try
            {
                List<KasaVM> kase = context.Kasa.Select(x => new KasaVM
                {
                    KasaId = x.KasaId,
                    Klijent = x.Klijent.Ime + "-" + x.Klijent.Alias,
                    KlijentId = x.KlijentId,
                    TipKase = x.TipKase.Naziv,
                    TipKaseId = x.TipKaseId
                }).Where(x => (klijentId != 0 ? x.KlijentId == klijentId : true))
                  .OrderBy(x => x.KasaId)
                  .ToList();

                if (searchInput != "")
                {
                    kase = kase.Where(x => x.Klijent.Contains(searchInput) || x.TipKase.Contains(searchInput)).ToList();
                }

                if (klijentId != 0)
                {
                    ViewBag.klijentId = klijentId;

                    return PartialView("Index_Partial", kase);
                }

                //pagging calculations (number of pages)
                decimal numberOfPages = (decimal)kase.Count / (decimal)itemsPerPage;
                ViewBag.numberOfPages = Math.Ceiling(numberOfPages);

                //set current page to make it visible on the html
                ViewBag.currentPage = currentPage;

                //pagging calculations (skip and take elements)
                kase = kase.Skip(currentPage * itemsPerPage).Take(itemsPerPage).ToList();
                Session["Selected"] = "KasaIndex";

                return View(kase);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // GET: Kasa/Create
        public ActionResult Create(int id = 0, int klijentId = 0)
        {
            try
            {
                KasaCreateVM kasaVM;
                if (klijentId != 0)
                {
                    kasaVM = new KasaCreateVM();
                    kasaVM.KlijentId = klijentId;
                    kasaVM.IsPartial = true;
                    kasaVM.TipoviKasa = context.TipKase.Select(x => new SelectListItem
                    {
                        Text = x.Naziv,
                        Value = x.TipKaseId.ToString()
                    }).ToList();

                    if (id != 0)
                    {
                        Kasa kasa = context.Kasa.Where(x => x.KasaId == id).FirstOrDefault();
                        kasaVM.KasaId = id;
                        kasaVM.KlijentId = kasa.KlijentId;
                        kasaVM.TipKaseId = kasa.TipKaseId;
                    }

                    ViewBag.allErrors = null;
                    return PartialView("Create_Partial", kasaVM);
                }

                kasaVM = new KasaCreateVM()
                {
                    Klijenti = context.Klijent.Select(x => new SelectListItem
                    {
                        Text = x.Ime + "-" + x.Alias,
                        Value = x.KlijentId.ToString()
                    }).ToList(),

                    TipoviKasa = context.TipKase.Select(x => new SelectListItem
                    {
                        Text = x.Naziv,
                        Value = x.TipKaseId.ToString()
                    }).ToList()
                };

                if (id != 0)
                {
                    Kasa kasa = context.Kasa.Where(x => x.KasaId == id).FirstOrDefault();
                    kasaVM.KasaId = kasa.KasaId;
                    kasaVM.KlijentId = kasa.KlijentId;
                    kasaVM.TipKaseId = kasa.TipKaseId;
                }

                return View(kasaVM);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // POST: Kasa/Create
        [HttpPost]
        public ActionResult Create(KasaCreateVM kasaVM)
        {
            List<string> allErrors = new List<string>();
            try
            {
                if (kasaVM.KasaId == 0)
                {
                    int index = context.Kasa.ToList().FindIndex(x => x.TipKaseId == kasaVM.TipKaseId && x.KlijentId == kasaVM.KlijentId);

                    if (index >= 0)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Kasa već postoji kod klijenta!");

                        if (kasaVM.IsPartial)
                        {
                            ViewBag.allErrors = allErrors;

                            kasaVM.Klijenti = context.Klijent.Select(x => new SelectListItem
                            {
                                Text = x.Ime + "-" + x.Alias,
                                Value = x.KlijentId.ToString()
                            }).ToList();

                            kasaVM.TipoviKasa = context.TipKase.Select(x => new SelectListItem
                            {
                                Text = x.Naziv,
                                Value = x.TipKaseId.ToString()
                            }).ToList();

                            return PartialView("Create_Partial", kasaVM);
                        }
                        return Json(allErrors);
                    }
                }

                #region Provjera da li je sve ispravno uneseno
                bool isInvalid = false;

                if (kasaVM.KlijentId == 0)
                {
                    isInvalid = true;
                    allErrors.Add("Klijent je obavezno polje!");
                }

                if (kasaVM.TipKaseId == 0)
                {
                    isInvalid = true;
                    allErrors.Add("Tip kase je obavezno polje!");
                }

                if (isInvalid)
                {
                    kasaVM.Klijenti = context.Klijent.Select(x => new SelectListItem
                    {
                        Text = x.Ime + "-" + x.Alias,
                        Value = x.KlijentId.ToString()
                    }).ToList();

                    kasaVM.TipoviKasa = context.TipKase.Select(x => new SelectListItem
                    {
                        Text = x.Naziv,
                        Value = x.TipKaseId.ToString()
                    }).ToList();

                    Response.AppendHeader("X-Error", "true");

                    if (kasaVM.IsPartial)
                    {
                        ViewBag.allErrors = allErrors;
                        return PartialView("Create_Partial", kasaVM);
                    }
                    return Json(allErrors);
                }
                #endregion

                Kasa kasa;

                if (kasaVM.KasaId == 0)
                {
                    //Kreira novu intervenciju i odma je posprema u bazu 
                    kasa = new Kasa();
                    context.Kasa.Add(kasa);
                }
                else
                {
                    //Vadi postojecu intervenciju za editovanje 
                    kasa = context.Kasa.Where(x => x.KasaId == kasaVM.KasaId).FirstOrDefault();
                }

                kasa.KlijentId = kasaVM.KlijentId;
                kasa.TipKaseId = kasaVM.TipKaseId;

                context.SaveChanges();

                if (kasaVM.IsPartial)
                    return Redirect("/Kasa/Index?klijentId=" + kasaVM.KlijentId);

                return Redirect("/Kasa/Index");
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // GET: Kasa/Delete/5
        public ActionResult Delete(int id, int klijentId)
        {
            try
            {
                context.Kasa.Remove(context.Kasa.Where(x => x.KasaId == id).SingleOrDefault());
                context.SaveChanges();

                if (klijentId != 0)
                    return Redirect("/Kasa/Index?klijentId=" + klijentId);

                return Redirect("/Kasa/Index");
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        [HttpPost]
        public ActionResult CreateTipKasePost(TipKaseCreateVM tipKaseVM)
        {
            try
            {
                List<string> allErrors = new List<string>();
                TipKase tipKase = context.TipKase.Where(x => x.Naziv.Equals(tipKaseVM.Naziv.Trim())).FirstOrDefault();

                if (tipKase != null)
                {
                    Response.AppendHeader("X-Error", "true");
                    allErrors.Add("Kasa već postoji!");
                    return Json(allErrors);
                }

                if (tipKaseVM.Naziv == null || tipKaseVM.Naziv.Trim() == String.Empty)
                {
                    Response.AppendHeader("X-Error", "true");
                    allErrors.Add("Naziv ne može biti prazan!");
                    return Json(allErrors);
                }

                tipKase = new TipKase();
                context.TipKase.Add(tipKase);
                tipKase.Naziv = tipKaseVM.Naziv.Trim();
                context.SaveChanges();

                return Json(tipKase);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }
    }
}
