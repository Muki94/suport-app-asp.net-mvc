﻿using DAL;
using DAL.Models;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SupportAplikacija.Controllers
{
    [Authorize]
    public class IntervencijaController : Controller
    {
        SupportContext context = new SupportContext();
        // GET: Intervencija
        public ActionResult Index(string searchInput = "", int itemsPerPage = 25, int currentPage = 0, int klijentId = 0)
        {
            try
            {
                List<IntervencijaVM> intervencije = context.Intervencija.Select(x => new IntervencijaVM
                {
                    IntervencijaId = x.IntervencijaId,
                    Opis = x.Opis.Substring(0, 200) + "...",
                    Datum = x.Datum,
                    Klijent = x.Klijent.Ime + "-" + x.Klijent.Alias,
                    KlijentId = x.KlijentId
                }).Where(x => (klijentId != 0 ? x.KlijentId == klijentId : true))
                  .OrderByDescending(x => x.Datum)
                  .ToList();

                if (searchInput != "")
                {
                    intervencije = intervencije.Where(x => x.Klijent.Contains(searchInput) || x.Opis.Contains(searchInput)).ToList();
                }

                if (klijentId != 0)
                {
                    ViewBag.klijentId = klijentId;

                    return PartialView("Index_Partial", intervencije);
                }

                //pagging calculations (number of pages)
                decimal numberOfPages = (decimal)intervencije.Count / (decimal)itemsPerPage;
                ViewBag.numberOfPages = Math.Ceiling(numberOfPages);

                //set current page to make it visible on the html
                ViewBag.currentPage = currentPage;

                //pagging calculations (skip and take elements)
                intervencije = intervencije.Skip(currentPage * itemsPerPage).Take(itemsPerPage).ToList();

                ViewBag.searchInput = searchInput;
                Session["Selected"] = "IntervencijaIndex";

                return View(intervencije);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // GET: Intervencija/Details/5
        public ActionResult Details(int id, int klijentId = 0)
        {
            try
            {
                IntervencijaVM intervencijaVM = context.Intervencija.Where(x => x.IntervencijaId == id).Select(x => new IntervencijaVM
                {
                    IntervencijaId = x.IntervencijaId,
                    Opis = x.Opis,
                    KlijentId = x.KlijentId,
                    Klijent = x.Klijent.Ime + "-" + x.Klijent.Alias,
                    Datum = x.Datum
                }).FirstOrDefault();

                if (klijentId != 0)
                {
                    return PartialView("Details_Partial", intervencijaVM);
                }

                return View(intervencijaVM);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        //id - intervencija id 
        // GET: Intervencija/Create
        public ActionResult Create(int id = 0, int klijentId = 0)
        {
            try
            {
                IntervencijaCreateVM intervencijaVM;
                #region handlanje slucaja ako je ajax poziv
                if (klijentId != 0)
                {
                    intervencijaVM = new IntervencijaCreateVM();
                    intervencijaVM.KlijentId = klijentId;
                    intervencijaVM.IsPartial = true;

                    if (id != 0)
                    {
                        Intervencija intervencija = context.Intervencija.Where(x => x.IntervencijaId == id).FirstOrDefault();
                        intervencijaVM.IntervencijaId = id;
                        intervencijaVM.Opis = intervencija.Opis;
                    }

                    ViewBag.allErrors = null;
                    return PartialView("Create_Partial", intervencijaVM);
                }
                #endregion

                intervencijaVM = new IntervencijaCreateVM()
                {
                    Klijenti = context.Klijent.Select(x => new SelectListItem
                    {
                        Text = x.Ime + "-" + x.Alias,
                        Value = x.KlijentId.ToString()
                    }).ToList()
                };

                if (id != 0)
                {
                    Intervencija intervencija = context.Intervencija.Where(x => x.IntervencijaId == id).FirstOrDefault();
                    intervencijaVM.IntervencijaId = intervencija.IntervencijaId;
                    intervencijaVM.Opis = intervencija.Opis;
                    intervencijaVM.KlijentId = intervencija.KlijentId;
                }

                return View(intervencijaVM);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // POST: Intervencija/Create
        [HttpPost]
        public ActionResult Create(IntervencijaCreateVM intervencijaVM)
        {
            Intervencija intervencija;

            try
            {
                if (intervencijaVM.IntervencijaId == 0)
                {
                    //Kreira novu intervenciju i odma je posprema u bazu 
                    intervencija = new Intervencija();
                    intervencija.Datum = DateTime.Now;
                    context.Intervencija.Add(intervencija);
                }
                else
                {
                    //Vadi postojecu intervenciju za editovanje 
                    intervencija = context.Intervencija.Where(x => x.IntervencijaId == intervencijaVM.IntervencijaId).FirstOrDefault();
                }

                #region Provjera da li je sve ispravno uneseno
                bool isInvalid = false;
                List<string> allErrors = new List<string>();

                if (intervencijaVM.KlijentId == 0)
                {
                    isInvalid = true;
                    allErrors.Add("Klijent je obavezno polje!");
                }

                if (intervencijaVM.Opis == null || intervencijaVM.Opis == String.Empty)
                {
                    isInvalid = true;
                    allErrors.Add("Opis je obavezno polje!");
                }

                if (isInvalid)
                {
                    Response.AppendHeader("X-Error", "true");

                    intervencijaVM.Klijenti = context.Klijent.Select(x => new SelectListItem
                               {
                                   Text = x.Ime + "-" + x.Alias,
                                   Value = x.KlijentId.ToString()
                               }).ToList();

                    if (intervencijaVM.IsPartial)
                    {
                        ViewBag.allErrors = allErrors;
                        return PartialView("Create_Partial", intervencijaVM);
                    }

                    return Json(allErrors);
                }
                #endregion

                intervencija.Opis = intervencijaVM.Opis;
                intervencija.KlijentId = intervencijaVM.KlijentId;

                context.SaveChanges();

                if (intervencijaVM.IsPartial)
                    return Redirect("/Intervencija/Index?klijentId=" + intervencijaVM.KlijentId);

                return Redirect("/Intervencija/Index");
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // GET: Intervencija/Delete/5
        public ActionResult Delete(int id, int klijentId = 0)
        {
            try
            {
                context.Intervencija.Remove(context.Intervencija.Where(x => x.IntervencijaId == id).SingleOrDefault());
                context.SaveChanges();

                if (klijentId != 0)
                    return Redirect("/Intervencija/Index?klijentId=" + klijentId);

                return Redirect("/Intervencija/Index");
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }
    }
}
