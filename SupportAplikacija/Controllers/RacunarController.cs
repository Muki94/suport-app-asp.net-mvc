﻿using DAL;
using DAL.Models;
using DAL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SupportAplikacija.Controllers
{
    [Authorize]
    public class RacunarController : Controller
    {
        SupportContext context = new SupportContext();
        // GET: Racunar
        public ActionResult Index(string searchInput = "", string sistemSelect = "", string processorSelect = "", int itemsPerPage = 25, int currentPage = 0, int klijentId = 0)
        {
            try
            {
                    List<RacunarVM> racunari = context.Racunar.Select(x => new RacunarVM
                    {
                        RacunarId = x.RacunarId,
                        KlijentId = x.KlijentId,
                        Klijent = x.Klijent.Ime + "-" + x.Klijent.Alias,
                        OperativniSistemId = x.OperativniSistem.OperativniSistemId,
                        OperativniSistem = x.OperativniSistem.Name,
                        ProcesorId = x.Procesor.ProcesorId,
                        Procesor = x.Procesor.Name,
                        TipRacunara = x.TipRacunara.Name,
                        RAM = x.RAM,
                        Graficka = x.Graficka.Name,
                        VrstaMemorije = x.VrstaMemorije.Name,
                        KolicinaMemorije = x.KolicinaMemorije

                    }).Where(x => (klijentId != 0 ? x.KlijentId == klijentId : true))
                      .OrderByDescending(x => x.Klijent)
                      .ToList();

                    ViewBag.ListaSistema = context.OperativniSistem.ToList();
                    ViewBag.ListaProcesora = context.Procesor.ToList();

                    if (searchInput != "")
                    {
                        racunari = racunari.Where(x => x.Klijent.Contains(searchInput)).ToList();
                    }

                    if (sistemSelect != "")
                    {
                        racunari = racunari.Where(x => x.OperativniSistemId.ToString().Equals(sistemSelect)).ToList();
                    }

                    if (processorSelect != "")
                    {
                        racunari = racunari.Where(x => x.ProcesorId.ToString().Equals(processorSelect)).ToList();
                    }

                    if (klijentId != 0)
                    {
                        ViewBag.klijentId = klijentId;

                        return PartialView("Index_Partial", racunari);
                    }

                    //pagging calculations (number of pages)
                    decimal numberOfPages = (decimal)racunari.Count / (decimal)itemsPerPage;
                    ViewBag.numberOfPages = Math.Ceiling(numberOfPages);

                    //set current page to make it visible on the html
                    ViewBag.currentPage = currentPage;

                    //pagging calculations (skip and take elements)
                    racunari = racunari.Skip(currentPage * itemsPerPage).Take(itemsPerPage).ToList();

                    ViewBag.selectedSistem = sistemSelect;
                    ViewBag.selectedProcessor = processorSelect;
                    ViewBag.searchInput = searchInput;
                    Session["Selected"] = "RacunarIndex";

                    return View(racunari);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // GET: Racunar/Details/5
        public ActionResult Details(int id, int klijentId = 0)
        {
            try
            {
                    RacunarVM racunarVM = context.Racunar.Where(x => x.RacunarId == id).Select(x => new RacunarVM
                    {
                        RacunarId = x.RacunarId,
                        Procesor = x.Procesor.Name,
                        OperativniSistem = x.OperativniSistem.Name,
                        KlijentId = x.KlijentId,
                        Klijent = x.Klijent.Ime + "-" + x.Klijent.Alias,
                        RAM = x.RAM,
                        KolicinaMemorije = x.KolicinaMemorije,
                        TipRacunara = x.TipRacunara.Name,
                        VrstaMemorije = x.VrstaMemorije.Name,
                        Graficka = x.Graficka.Name
                    }).FirstOrDefault();

                    if (klijentId != 0)
                    {
                        return PartialView("Details_Partial", racunarVM);
                    }

                    return View(racunarVM);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // GET: Racunar/Create
        public ActionResult Create(int id = 0, int klijentId = 0)
        {
            try
            {
                RacunaCreateVM racunarVM = new RacunaCreateVM();
                    InitializeDropdowns(racunarVM);

                    if (id != 0)
                    {
                        Racunar racunar = context.Racunar.Where(x => x.RacunarId == id).FirstOrDefault();
                        racunarVM.KlijentId = racunar.KlijentId;
                        racunarVM.RacunarId = id;
                        racunarVM.OperativniSistemId = racunar.OperativniSistemId;
                        racunarVM.ProcesorId = racunar.ProcesorId;
                        racunarVM.TipRacunaraId = racunar.TipRacunaraId;
                        racunarVM.RAM = racunar.RAM;
                        racunarVM.GrafickaId = racunar.GrafickaId;
                        racunarVM.VrstaMemorijeId = racunar.VrstaMemorijeId;
                        racunarVM.KolicinaMemorije = racunar.KolicinaMemorije;
                    }

                    if (klijentId != 0)
                    {
                        racunarVM.KlijentId = klijentId;
                        racunarVM.IsPartial = true;

                        ViewBag.allErrors = null;
                        return PartialView("Create_Partial", racunarVM);
                    }

                    racunarVM.Klijenti = context.Klijent.Select(x => new SelectListItem
                    {
                        Text = x.Ime,
                        Value = x.KlijentId.ToString()
                    }).ToList();

                    return View(racunarVM);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // POST: Racunar/Create
        [HttpPost]
        public ActionResult Create(RacunaCreateVM racunarVM)
        {
            try
            {
                Racunar racunar;

                    if (racunarVM.RacunarId == 0)
                    {
                        //Kreira novu intervenciju i odma je posprema u bazu 
                        racunar = new Racunar();
                        context.Racunar.Add(racunar);
                    }
                    else
                    {
                        //Vadi postojecu intervenciju za editovanje 
                        racunar = context.Racunar.Where(x => x.RacunarId == racunarVM.RacunarId).FirstOrDefault();
                    }

                    #region Provjera da li je sve ispravno uneseno

                    List<string> allErrors = new List<string>();

                    if (!ModelState.IsValid)
                    {
                        Response.AppendHeader("X-Error", "true");

                        foreach (ModelState modelState in ViewData.ModelState.Values)
                        {
                            foreach (ModelError error in modelState.Errors)
                            {
                                allErrors.Add(error.ErrorMessage.ToString());
                            }
                        }

                        InitializeDropdowns(racunarVM);
                        
                        if (racunarVM.IsPartial)
                        {
                            ViewBag.allErrors = allErrors;
                            return PartialView("Create_Partial", racunarVM);
                        }

                        return Json(allErrors);
                    }
    
                    #endregion

                    racunar.OperativniSistemId = racunarVM.OperativniSistemId;
                    racunar.TipRacunaraId = racunarVM.TipRacunaraId;
                    racunar.ProcesorId = racunarVM.ProcesorId;
                    racunar.KlijentId = racunarVM.KlijentId;
                    racunar.RAM = racunarVM.RAM;
                    racunar.GrafickaId = racunarVM.GrafickaId;
                    racunar.VrstaMemorijeId = racunarVM.VrstaMemorijeId;
                    racunar.KolicinaMemorije = racunarVM.KolicinaMemorije;

                    context.SaveChanges();

                    if (racunarVM.IsPartial)
                        return Redirect("/Racunar/Index?klijentId=" + racunarVM.KlijentId);

                    return Redirect("/Racunar/Index");
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        // POST: Racunar/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, int klijentId = 0)
        {
            try
            {
                    context.Racunar.Remove(context.Racunar.Where(x => x.RacunarId == id).FirstOrDefault());
                    context.SaveChanges();

                    if (klijentId != 0)
                        return Redirect("/Racunar/Index?klijentId=" + klijentId);

                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        [HttpPost]
        public ActionResult CreateTipRacunaraPost(TipRacunaraCreateVM tipRacunaraVM)
        {
            try
            {
                    List<string> allErrors = new List<string>();
                    TipRacunara tipRacunara = context.TipRacunara.Where(x => x.Name.Equals(tipRacunaraVM.Naziv.Trim())).FirstOrDefault();

                    if (tipRacunara != null)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Tip računara već postoji!");
                        return Json(allErrors);
                    }

                    if (tipRacunaraVM.Naziv == null || tipRacunaraVM.Naziv.Trim() == String.Empty)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Naziv ne može biti prazan!");
                        return Json(allErrors);
                    }

                    tipRacunara = new TipRacunara();
                    context.TipRacunara.Add(tipRacunara);
                    tipRacunara.Name = tipRacunaraVM.Naziv.Trim();
                    context.SaveChanges();

                    return Json(tipRacunara);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        [HttpPost]
        public ActionResult CreateOperativniSistemPost(OperativniSistemCreateVM operativniSistemVM)
        {
            try
            {
                    List<string> allErrors = new List<string>();
                    OperativniSistem operativniSistem = context.OperativniSistem.Where(x => x.Name.Equals(operativniSistemVM.Naziv.Trim())).FirstOrDefault();

                    if (operativniSistem != null)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Operativni sistem već postoji!");
                        return Json(allErrors);
                    }

                    if (operativniSistemVM.Naziv == null || operativniSistemVM.Naziv.Trim() == String.Empty)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Naziv ne može biti prazan!");
                        return Json(allErrors);
                    }

                    operativniSistem = new OperativniSistem();
                    context.OperativniSistem.Add(operativniSistem);
                    operativniSistem.Name = operativniSistemVM.Naziv.Trim();
                    context.SaveChanges();

                    return Json(operativniSistem);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        [HttpPost]
        public ActionResult CreateProcesorPost(ProcesorCreateVM procesorVM)
        {
            try
            {
                    List<string> allErrors = new List<string>();
                    Procesor procesor = context.Procesor.Where(x => x.Name.Equals(procesorVM.Naziv.Trim())).FirstOrDefault();

                    if (procesor != null)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Procesor već postoji!");
                        return Json(allErrors);
                    }

                    if (procesorVM.Naziv == null || procesorVM.Naziv.Trim() == String.Empty)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Naziv ne može biti prazan!");
                        return Json(allErrors);
                    }

                    procesor = new Procesor();
                    context.Procesor.Add(procesor);
                    procesor.Name = procesorVM.Naziv.Trim();
                    context.SaveChanges();

                    return Json(procesor);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        [HttpPost]
        public ActionResult CreateGrafickaPost(GrafickaCreateVM grafickaVM)
        {
            try
            {
                    List<string> allErrors = new List<string>();
                    Graficka graficka = context.Graficka.Where(x => x.Name.Equals(grafickaVM.Naziv.Trim())).FirstOrDefault();

                    if (graficka != null)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Grafička već postoji!");
                        return Json(allErrors);
                    }

                    if (grafickaVM.Naziv == null || grafickaVM.Naziv.Trim() == String.Empty)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Naziv ne može biti prazan!");
                        return Json(allErrors);
                    }

                    graficka = new Graficka();
                    context.Graficka.Add(graficka);
                    graficka.Name = grafickaVM.Naziv.Trim();
                    context.SaveChanges();

                    return Json(graficka);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        [HttpPost]
        public ActionResult CreateVrstaMemorijePost(VrstaMemorijeCreateVM vrstaMemorijeVM)
        {
            try
            {
                    List<string> allErrors = new List<string>();
                    VrstaMemorije vrstaMemorije = context.VrstaMemorije.Where(x => x.Name.Equals(vrstaMemorijeVM.Naziv.Trim())).FirstOrDefault();

                    if (vrstaMemorije != null)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Vrsta memorije već postoji!");
                        return Json(allErrors);
                    }

                    if (vrstaMemorijeVM.Naziv == null || vrstaMemorijeVM.Naziv.Trim() == String.Empty)
                    {
                        Response.AppendHeader("X-Error", "true");
                        allErrors.Add("Naziv ne može biti prazan!");
                        return Json(allErrors);
                    }

                    vrstaMemorije = new VrstaMemorije();
                    context.VrstaMemorije.Add(vrstaMemorije);
                    vrstaMemorije.Name = vrstaMemorijeVM.Naziv.Trim();
                    context.SaveChanges();

                    return Json(vrstaMemorije);
            }
            catch (Exception exception)
            {
                ViewBag.fatalError = exception;

                return PartialView("ErrorPage");
            }
        }

        private void InitializeDropdowns(RacunaCreateVM racunarVM)
        {

            racunarVM.TipRacunara = context.TipRacunara.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.TipRacunaraId.ToString()
            }).ToList();

            racunarVM.Procesor = context.Procesor.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.ProcesorId.ToString()
            }).ToList();

            racunarVM.Graficka = context.Graficka.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.GrafickaId.ToString()
            }).ToList();

            racunarVM.VrstaMemorije = context.VrstaMemorije.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.VrstaMemorijeId.ToString()
            }).ToList();

            racunarVM.OperativniSistem = context.OperativniSistem.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.OperativniSistemId.ToString()
            }).ToList();
        }
    }
}
