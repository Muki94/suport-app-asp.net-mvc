﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class TipKaseCreateVM
    {
        public int TipKaseId { get; set; }

        public string Naziv { get; set; }
    }
}
