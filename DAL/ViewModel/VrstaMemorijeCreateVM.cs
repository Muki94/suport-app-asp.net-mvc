﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class VrstaMemorijeCreateVM
    {
        public int VrstaMemorijeId { get; set; }

        public string Naziv { get; set; }

        public bool IsPartial { get; set; }
        
        public int KlijentId { get; set; }
    }
}
