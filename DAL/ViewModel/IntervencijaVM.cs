﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class IntervencijaVM
    {
        public int IntervencijaId { get; set; }

        public DateTime Datum { get; set; }

        public string Opis { get; set; }

        public string Klijent{ get; set; }

        public int KlijentId { get; set; }
    }
}
