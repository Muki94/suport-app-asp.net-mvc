﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class TipRacunaraCreateVM
    {
        public int TipRacunaraId { get; set; }

        public string Naziv { get; set; }

        public int KlijentId { get; set; }

        public bool IsPartial { get; set; }

    }
}
