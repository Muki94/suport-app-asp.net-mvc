﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAL.ViewModel
{
    public class KasaCreateVM
    {
        public int KasaId { get; set; }

        public int KlijentId { get; set; }
        public IEnumerable<SelectListItem> Klijenti { get; set; }

        public int TipKaseId { get; set; }
        public IEnumerable<SelectListItem> TipoviKasa { get; set; }

        public bool IsPartial { get; set; }
    }
}
