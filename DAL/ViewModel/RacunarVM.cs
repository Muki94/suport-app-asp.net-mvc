﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAL.ViewModel
{
    public class RacunarVM
    {
        public int RacunarId { get; set; }

        public string TipRacunara { get; set; }

        public int ProcesorId { get; set; }
        public string Procesor { get; set; }

        public int OperativniSistemId { get; set; }
        public string OperativniSistem { get; set; }
        
        public string RAM { get; set; }

        public string Graficka { get; set; }

        public string VrstaMemorije { get; set; }

        public string KolicinaMemorije { get; set; }

        public int KlijentId { get; set; }
        public string Klijent { get; set; }
    }
}
