﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAL.ViewModel
{
    public class KlijentCreateVM
    {
        public int KlijentId { get; set; }

        [Required(ErrorMessage="Ime Klijenta je obavezno polje")]
        [RegularExpression(@"^[0-9a-zA-Z' 'šđčćžŠĐČĆŽ]{1,40}$",ErrorMessage = "Specijalni karakteri nisu dozvoljeni!")]
        public string Ime { get; set; }

        public string Alias { get; set; }

        [Required(ErrorMessage = "Telefonski broj je obavezno polje")]
        [MaxLength(12, ErrorMessage = "Telefonski broj nije u ispravnom formatu")]
        [MinLength(9, ErrorMessage = "Telefonski broj nije u ispravnom formatu")]
        [Phone(ErrorMessage = "Telefonski broj nije u ispravnom formatu")]
        public string Telefon { get; set; }

        [Required(ErrorMessage = "Adresa je obavezno polje")]
        public string Adresa { get; set; }

        public string Napomena { get; set; }

        [Required(ErrorMessage = "Grad je obavezno polje")]
        public int GradId { get; set; }
        public IEnumerable<SelectListItem> Gradovi { get; set; }
    }
}
