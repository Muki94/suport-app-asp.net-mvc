﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class SlikaVM
    {
        public int SlikaId { get; set; }

        public string PutanjaDoSlike { get; set; }

        public string Napomena { get; set; }

        public int KlijentId { get; set; }
    }
}
