﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAL.ViewModel
{
    public class RacunaCreateVM
    {
        public int RacunarId { get; set; }

        [Required(ErrorMessage = "Tip računara je obavezno polje")]
        public int TipRacunaraId { get; set; }
        public IEnumerable<SelectListItem> TipRacunara { get; set; }

        [Required(ErrorMessage = "Procesor je obavezno polje")]
        public int ProcesorId { get; set; }
        public IEnumerable<SelectListItem> Procesor { get; set; }

        [Required(ErrorMessage = "Operativni sistem je obavezno polje")]
        public int OperativniSistemId { get; set; }
        public IEnumerable<SelectListItem> OperativniSistem { get; set; }

        [Required(ErrorMessage = "RAM je obavezno polje")]
        public string RAM { get; set; }

        [Required(ErrorMessage = "Grafička je obavezno polje")]
        public int GrafickaId { get; set; }
        public IEnumerable<SelectListItem> Graficka { get; set; }

        [Required(ErrorMessage = "Vrsta memorije je obavezno polje")]
        public int VrstaMemorijeId { get; set; }
        public IEnumerable<SelectListItem> VrstaMemorije { get; set; }

        [Required(ErrorMessage = "Količina memorije je obavezno polje")]
        public string KolicinaMemorije { get; set; }

        [Required(ErrorMessage = "Klijent je obavezno polje")]
        public int KlijentId { get; set; }
        public IEnumerable<SelectListItem> Klijenti { get; set; }

        public bool IsPartial { get; set; }
    }
}
