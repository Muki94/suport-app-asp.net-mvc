﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class KasaVM
    {
        public int KasaId { get; set; }

        public int KlijentId { get; set; }
        public string Klijent { get; set; }

        public int TipKaseId { get; set; }
        public string TipKase { get; set; }
    }
}
