﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModel
{
    public class KorisnikCreateVM
    {
        public int KorisnikId { get; set; }

        [Required(ErrorMessage = "Ime je obavezno polje")]
        public string Ime { get; set; }
        
        [Required(ErrorMessage = "Prezime je obavezno polje")]
        public string Prezime { get; set; }

        [Required(ErrorMessage = "Username je obavezno polje")]
        public string UserName { get; set; }

        public string Password { get; set; }

        public string StariPassword { get; set; }

        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Email nije unesen u ispravnom formatu")]
        [Required(ErrorMessage = "Email je obavezno polje")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telefonski broj je obavezno polje")]
        [MaxLength(12, ErrorMessage = "Telefonski broj nije u ispravnom formatu")]
        [MinLength(9, ErrorMessage = "Telefonski broj nije u ispravnom formatu")]
        [Phone(ErrorMessage = "Telefonski broj nije u ispravnom formatu")]
        public string Telefon { get; set; }
    }
}
