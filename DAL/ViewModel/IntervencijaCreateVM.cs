﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAL.ViewModel
{
    public class IntervencijaCreateVM
    {
        public int IntervencijaId { get; set; }

        [Required(ErrorMessage = "Opis je obavezno polje")]
        public string Opis { get; set; }

        public int KlijentId { get; set; }
        public IEnumerable<SelectListItem> Klijenti { get; set; }

        public bool IsPartial { get; set; }
    }
}
