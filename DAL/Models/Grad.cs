﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Models
{
    public class Grad
    {
        public int GradId { get; set; }

        public string Naziv { get; set; }
    }
}