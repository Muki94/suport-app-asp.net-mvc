﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class VrstaMemorije
    {
        public int VrstaMemorijeId { get; set; }

        public string Name { get; set; }
    }
}
