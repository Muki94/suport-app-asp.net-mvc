﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Slika
    {
        public int SlikaId { get; set; }

        public string PutanjaDoSlike { get; set; }

        public string Napomena { get; set; }

        public int KlijentId { get; set; }
        public virtual Klijent Klijent { get; set; }
    }
}
