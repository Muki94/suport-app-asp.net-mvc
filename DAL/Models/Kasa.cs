﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Models
{
    public class Kasa
    {
        public int KasaId { get; set; }

        public int KlijentId { get; set; }
        public virtual Klijent Klijent { get; set; }

        public int TipKaseId { get; set; }
        public virtual TipKase TipKase { get; set; }
    }
}