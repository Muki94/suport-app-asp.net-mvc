﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Models
{
    public class Racunar
    {
        public int RacunarId { get; set; }

        public int TipRacunaraId { get; set; }
        public virtual TipRacunara TipRacunara { get; set; }

        public int ProcesorId { get; set; }
        public virtual Procesor Procesor { get; set; }

        public int OperativniSistemId { get; set; }
        public virtual OperativniSistem OperativniSistem { get; set; }

        public string RAM { get; set; }

        public int GrafickaId { get; set; }
        public virtual Graficka Graficka { get; set; }

        public int VrstaMemorijeId { get; set; }
        public virtual VrstaMemorije VrstaMemorije { get; set; }

        public string KolicinaMemorije { get; set; }

        public int KlijentId { get; set; }
        public virtual Klijent Klijent { get; set; }
    }
}