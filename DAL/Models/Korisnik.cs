﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Korisnik
    {
        public int KorisnikId { get; set; }

        public string Ime { get; set; }

        public string Prezime { get; set; }

        public string UserName { get; set; }
        
        public string Password { get; set; }
        
        public string Email { get; set; }

        public string Telefon { get; set; }
    }
}
