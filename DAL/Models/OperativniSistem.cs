﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class OperativniSistem
    {
        public int OperativniSistemId { get; set; }

        public string Name { get; set; }
    }
}
