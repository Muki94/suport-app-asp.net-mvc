﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Models
{
    public class Intervencija
    {
        public int IntervencijaId { get; set; }

        public DateTime Datum { get; set; }

        public string Opis { get; set; }

        public int KlijentId { get; set; }
        public virtual Klijent Klijent { get; set; }
    }
}