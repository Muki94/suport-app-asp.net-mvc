﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Models
{
    public class Klijent
    {
        public int KlijentId { get; set; }

        public string Ime { get; set; }

        public string Alias { get; set; }

        public string Telefon { get; set; }

        public string Adresa { get; set; }

        public DateTime DatumDodavanja { get; set; }

        public DateTime DatumUredjivanja { get; set; }

        public string Napomena { get; set; }

        public int GradId { get; set; }
        public virtual Grad Grad { get; set; }
    }
}