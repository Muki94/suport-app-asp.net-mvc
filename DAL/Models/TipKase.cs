﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.Models
{
    public class TipKase
    {
        public int TipKaseId { get; set; }

        public string Naziv { get; set; }
    }
}