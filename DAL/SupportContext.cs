﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL
{
    public class SupportContext:DbContext
    {
        public SupportContext():base("SupportConectionString")
        {

        }

        public DbSet<Grad> Grad { get; set; }
        public DbSet<Intervencija> Intervencija { get; set; }
        public DbSet<Kasa> Kasa { get; set; }
        public DbSet<Klijent> Klijent { get; set; }
        public DbSet<Korisnik> Korisnik { get; set; }
        public DbSet<Racunar> Racunar { get; set; }
        public DbSet<TipKase> TipKase { get; set; }
        public DbSet<Slika> Slika { get; set; }
        public DbSet<TipRacunara> TipRacunara { get; set; }
        public DbSet<Procesor> Procesor { get; set; }
        public DbSet<Graficka> Graficka { get; set; }
        public DbSet<VrstaMemorije> VrstaMemorije { get; set; }
        public DbSet<OperativniSistem> OperativniSistem { get; set; }
    }
}
