namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PromjenjenitippodatkaOperatingSystemsuOperativnisistemi : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Racunars", "OperativniSistemId");
            AddForeignKey("dbo.Racunars", "OperativniSistemId", "dbo.OperativniSistems", "OperativniSistemId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Racunars", "OperativniSistemId", "dbo.OperativniSistems");
            DropIndex("dbo.Racunars", new[] { "OperativniSistemId" });
        }
    }
}
