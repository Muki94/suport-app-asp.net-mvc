namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IzmjenjentippodatkaatributanapomenaizklaseSlika : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Slikas", "Napomena", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Slikas", "Napomena", c => c.Int(nullable: false));
        }
    }
}
