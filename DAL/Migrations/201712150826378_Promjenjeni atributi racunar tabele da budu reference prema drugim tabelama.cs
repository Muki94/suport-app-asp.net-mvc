namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Promjenjeniatributiracunartabeledabudureferencepremadrugimtabelama : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.KlijentSlikas", "KlijentId", "dbo.Klijents");
            DropIndex("dbo.KlijentSlikas", new[] { "KlijentId" });
            CreateTable(
                "dbo.Grafickas",
                c => new
                    {
                        GrafickaId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.GrafickaId);
            
            CreateTable(
                "dbo.OperativniSistems",
                c => new
                    {
                        OperativniSistemId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.OperativniSistemId);
            
            CreateTable(
                "dbo.Procesors",
                c => new
                    {
                        ProcesorId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ProcesorId);
            
            CreateTable(
                "dbo.TipRacunaras",
                c => new
                    {
                        TipRacunaraId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.TipRacunaraId);
            
            CreateTable(
                "dbo.VrstaMemorijes",
                c => new
                    {
                        VrstaMemorijeId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.VrstaMemorijeId);
            
            AddColumn("dbo.Racunars", "TipRacunaraId", c => c.Int(nullable: false));
            AddColumn("dbo.Racunars", "ProcesorId", c => c.Int(nullable: false));
            AddColumn("dbo.Racunars", "OperativniSistemId", c => c.Int(nullable: false));
            AddColumn("dbo.Racunars", "GrafickaId", c => c.Int(nullable: false));
            AddColumn("dbo.Racunars", "VrstaMemorijeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Racunars", "TipRacunaraId");
            CreateIndex("dbo.Racunars", "ProcesorId");
            CreateIndex("dbo.Racunars", "GrafickaId");
            CreateIndex("dbo.Racunars", "VrstaMemorijeId");
            AddForeignKey("dbo.Racunars", "GrafickaId", "dbo.Grafickas", "GrafickaId", cascadeDelete: true);
            AddForeignKey("dbo.Racunars", "ProcesorId", "dbo.Procesors", "ProcesorId", cascadeDelete: true);
            AddForeignKey("dbo.Racunars", "TipRacunaraId", "dbo.TipRacunaras", "TipRacunaraId", cascadeDelete: true);
            AddForeignKey("dbo.Racunars", "VrstaMemorijeId", "dbo.VrstaMemorijes", "VrstaMemorijeId", cascadeDelete: true);
            DropColumn("dbo.Racunars", "TipRacunara");
            DropColumn("dbo.Racunars", "Procesor");
            DropColumn("dbo.Racunars", "OperativniSistem");
            DropColumn("dbo.Racunars", "Graficka");
            DropColumn("dbo.Racunars", "VrstaMemorije");
            DropTable("dbo.KlijentSlikas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.KlijentSlikas",
                c => new
                    {
                        KlijentSlikaId = c.Int(nullable: false, identity: true),
                        Slika = c.String(),
                        Biljeska = c.String(),
                        KlijentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KlijentSlikaId);
            
            AddColumn("dbo.Racunars", "VrstaMemorije", c => c.String());
            AddColumn("dbo.Racunars", "Graficka", c => c.String());
            AddColumn("dbo.Racunars", "OperativniSistem", c => c.String());
            AddColumn("dbo.Racunars", "Procesor", c => c.String());
            AddColumn("dbo.Racunars", "TipRacunara", c => c.String());
            DropForeignKey("dbo.Racunars", "VrstaMemorijeId", "dbo.VrstaMemorijes");
            DropForeignKey("dbo.Racunars", "TipRacunaraId", "dbo.TipRacunaras");
            DropForeignKey("dbo.Racunars", "ProcesorId", "dbo.Procesors");
            DropForeignKey("dbo.Racunars", "GrafickaId", "dbo.Grafickas");
            DropIndex("dbo.Racunars", new[] { "VrstaMemorijeId" });
            DropIndex("dbo.Racunars", new[] { "GrafickaId" });
            DropIndex("dbo.Racunars", new[] { "ProcesorId" });
            DropIndex("dbo.Racunars", new[] { "TipRacunaraId" });
            DropColumn("dbo.Racunars", "VrstaMemorijeId");
            DropColumn("dbo.Racunars", "GrafickaId");
            DropColumn("dbo.Racunars", "OperativniSistemId");
            DropColumn("dbo.Racunars", "ProcesorId");
            DropColumn("dbo.Racunars", "TipRacunaraId");
            DropTable("dbo.VrstaMemorijes");
            DropTable("dbo.TipRacunaras");
            DropTable("dbo.Procesors");
            DropTable("dbo.OperativniSistems");
            DropTable("dbo.Grafickas");
            CreateIndex("dbo.KlijentSlikas", "KlijentId");
            AddForeignKey("dbo.KlijentSlikas", "KlijentId", "dbo.Klijents", "KlijentId", cascadeDelete: true);
        }
    }
}
