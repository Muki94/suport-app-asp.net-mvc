namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodatiatributiRAMGrafickaMemorijaiKolicinaMemorijeuklasuRacunar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Racunars", "RAM", c => c.String());
            AddColumn("dbo.Racunars", "Graficka", c => c.String());
            AddColumn("dbo.Racunars", "VrstaMemorije", c => c.String());
            AddColumn("dbo.Racunars", "KolicinaMemorije", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Racunars", "KolicinaMemorije");
            DropColumn("dbo.Racunars", "VrstaMemorije");
            DropColumn("dbo.Racunars", "Graficka");
            DropColumn("dbo.Racunars", "RAM");
        }
    }
}
