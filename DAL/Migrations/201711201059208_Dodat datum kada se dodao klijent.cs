namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dodatdatumkadasedodaoklijent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Klijents", "DatumDodavanja", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Klijents", "DatumDodavanja");
        }
    }
}
