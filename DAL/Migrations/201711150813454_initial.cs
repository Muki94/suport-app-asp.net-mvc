namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Grads",
                c => new
                    {
                        GradId = c.Int(nullable: false, identity: true),
                        Naziv = c.String(),
                    })
                .PrimaryKey(t => t.GradId);
            
            CreateTable(
                "dbo.Intervencijas",
                c => new
                    {
                        IntervencijaId = c.Int(nullable: false, identity: true),
                        Datum = c.DateTime(nullable: false),
                        Opis = c.String(),
                        KlijentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IntervencijaId)
                .ForeignKey("dbo.Klijents", t => t.KlijentId, cascadeDelete: true)
                .Index(t => t.KlijentId);
            
            CreateTable(
                "dbo.Klijents",
                c => new
                    {
                        KlijentId = c.Int(nullable: false, identity: true),
                        Ime = c.String(),
                        Alias = c.String(),
                        Telefon = c.String(),
                        Adresa = c.String(),
                        GradId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KlijentId)
                .ForeignKey("dbo.Grads", t => t.GradId, cascadeDelete: true)
                .Index(t => t.GradId);
            
            CreateTable(
                "dbo.Kasas",
                c => new
                    {
                        KasaId = c.Int(nullable: false, identity: true),
                        KlijentId = c.Int(nullable: false),
                        TipKaseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KasaId)
                .ForeignKey("dbo.Klijents", t => t.KlijentId, cascadeDelete: true)
                .ForeignKey("dbo.TipKases", t => t.TipKaseId, cascadeDelete: true)
                .Index(t => t.KlijentId)
                .Index(t => t.TipKaseId);
            
            CreateTable(
                "dbo.TipKases",
                c => new
                    {
                        TipKaseId = c.Int(nullable: false, identity: true),
                        Naziv = c.String(),
                    })
                .PrimaryKey(t => t.TipKaseId);
            
            CreateTable(
                "dbo.KlijentSlikas",
                c => new
                    {
                        KlijentSlikaId = c.Int(nullable: false, identity: true),
                        Slika = c.String(),
                        Biljeska = c.String(),
                        KlijentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KlijentSlikaId)
                .ForeignKey("dbo.Klijents", t => t.KlijentId, cascadeDelete: true)
                .Index(t => t.KlijentId);
            
            CreateTable(
                "dbo.Korisniks",
                c => new
                    {
                        KorisnikId = c.Int(nullable: false, identity: true),
                        Ime = c.String(),
                        Prezime = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        Telefon = c.String(),
                    })
                .PrimaryKey(t => t.KorisnikId);
            
            CreateTable(
                "dbo.Racunars",
                c => new
                    {
                        RacunarId = c.Int(nullable: false, identity: true),
                        TipRacunara = c.String(),
                        Procesor = c.String(),
                        OperativniSistem = c.String(),
                        KlijentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RacunarId)
                .ForeignKey("dbo.Klijents", t => t.KlijentId, cascadeDelete: true)
                .Index(t => t.KlijentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Racunars", "KlijentId", "dbo.Klijents");
            DropForeignKey("dbo.KlijentSlikas", "KlijentId", "dbo.Klijents");
            DropForeignKey("dbo.Kasas", "TipKaseId", "dbo.TipKases");
            DropForeignKey("dbo.Kasas", "KlijentId", "dbo.Klijents");
            DropForeignKey("dbo.Intervencijas", "KlijentId", "dbo.Klijents");
            DropForeignKey("dbo.Klijents", "GradId", "dbo.Grads");
            DropIndex("dbo.Racunars", new[] { "KlijentId" });
            DropIndex("dbo.KlijentSlikas", new[] { "KlijentId" });
            DropIndex("dbo.Kasas", new[] { "TipKaseId" });
            DropIndex("dbo.Kasas", new[] { "KlijentId" });
            DropIndex("dbo.Klijents", new[] { "GradId" });
            DropIndex("dbo.Intervencijas", new[] { "KlijentId" });
            DropTable("dbo.Racunars");
            DropTable("dbo.Korisniks");
            DropTable("dbo.KlijentSlikas");
            DropTable("dbo.TipKases");
            DropTable("dbo.Kasas");
            DropTable("dbo.Klijents");
            DropTable("dbo.Intervencijas");
            DropTable("dbo.Grads");
        }
    }
}
