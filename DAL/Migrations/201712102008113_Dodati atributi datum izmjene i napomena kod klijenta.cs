namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dodatiatributidatumizmjeneinapomenakodklijenta : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Klijents", "DatumUredjivanja", c => c.DateTime(nullable: false));
            AddColumn("dbo.Klijents", "Napomena", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Klijents", "Napomena");
            DropColumn("dbo.Klijents", "DatumUredjivanja");
        }
    }
}
