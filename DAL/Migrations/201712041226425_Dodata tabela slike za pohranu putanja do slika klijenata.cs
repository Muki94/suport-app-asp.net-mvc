namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dodatatabelaslikezapohranuputanjadoslikaklijenata : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Slikas",
                c => new
                    {
                        SlikaId = c.Int(nullable: false, identity: true),
                        PutanjaDoSlike = c.String(),
                        Napomena = c.Int(nullable: false),
                        KlijentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SlikaId)
                .ForeignKey("dbo.Klijents", t => t.KlijentId, cascadeDelete: true)
                .Index(t => t.KlijentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Slikas", "KlijentId", "dbo.Klijents");
            DropIndex("dbo.Slikas", new[] { "KlijentId" });
            DropTable("dbo.Slikas");
        }
    }
}
